/*
 这个类用于与CNNIC的ONS的通信
 */

#ifndef ONSMESSENGER_H
#define ONSMESSENGER_H

#include <QObject>
#include <QBuffer>
#include <QXmlStreamReader>

class ONSMessenger : public QObject
{
    Q_OBJECT
public:
    explicit ONSMessenger(QString iniMyURI,QObject *parent = 0);

    //根据参数，生成用于发送给ONS的报文
    //paraID是资源的数字标识，可以为mac地址
    QByteArray genMesg(QString paraURI_ObjName,QString paraURI_ObjType,
         QString paraURI_ObjOwner,QString paraURI_ObjLocation,
         QString paraServiceType,QString paraID,
         bool paraIsGW=true,QString paraPWD="");
    //对接收到的ONS的信息进行处理。结果存入数据域的各个变量.返回值为
    //错误信息或者”Success“
    QString analyzeReceivedMsg(QByteArray paraOnsMsg);


signals:

public slots:

private:
    //根据参数生成一条URI，结果存储在数据域的URI变量中
    void    generateURI(QString paraURI_ObjName,QString paraURI_ObjType,
                QString paraURI_ObjOwner,QString paraURI_ObjLocation);

    //对数据域的XMLBuffer进行解析
    QString    parseXML();

    /*
    generateXML()根据数据域的各个变量，生成XML，存储于数据域的XMLBuffer
    参数： isGW
    说明： 若为true，表示生成的xml信息将由网关发送给ONS。
    若为false，表示生成的xml信息将由普通客户端发送给ONS。
    */
    void    generateXML( QString paraServiceType,QString paraID,
                         bool paraIsGW=true,QString paraPWD="");
    //对数据域的数据复位
    void    resetDate();
    /*
     下面的一系列read函数，是用于解析xml文档，被parseXML()函数调用
     */
    void    readVideoMonitorElem();
    void    readSerialNumberElem();
    void    readClientNameElem();
    void    readGWURIElem();
    void    readRecordElem();
    void    readServiceTypeElem();
    void    readURIElem();
    void    readIDElem();
    void    readGWIPElem();
    void    readGWPortElem();
    void    readPassWordElem();
    void    readStatusElem();
    void    readDescriptionElem();
    void    skipUnknownElem();

    QXmlStreamReader    xmlReader;
    QXmlStreamWriter    xmlWriter;

public:
    /*
      下面的一些变量，除了MyURI外，用于存储接收到的ONS消息的解析结果
      */
    QBuffer XMLBuffer; //接收到的xml文档或将要发送的xml文档
    QString ApplicationType; //such as "VideoMonitor"
    quint8  SerialNumer;
    //ServiceType：Register，Query，Update，Delete，Response，区分大小写。
    QString ServiceType;
    QString ID;
    QString PassWord;
    QString Status;
    QString GWIP;
    QString GWPort;
    QString Description;
    QString URI;

    //MyURI记录程序自己的URI。如果是网关使用这个onsmessenger，
    //那么这个就是网关自己的URI
    QString MyURI;
};

#endif // ONSMESSENGER_H
