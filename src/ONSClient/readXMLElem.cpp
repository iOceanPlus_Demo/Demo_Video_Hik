#include "onsmessenger.h"

void    ONSMessenger::readVideoMonitorElem()
{
    xmlReader.readNext();
    while(!xmlReader.atEnd())
    {
        if(xmlReader.isEndElement())
        {
            xmlReader.readNext();
            break;
        }
        if(xmlReader.isStartElement())
        {
            if(xmlReader.name()=="SerialNumber")
                readSerialNumberElem();
            else  if(xmlReader.name()=="ClientName")
                readClientNameElem();
            else  if(xmlReader.name()=="GW-URI")
                readGWURIElem();
            else  if(xmlReader.name()=="Record")
                readRecordElem();
            else
                skipUnknownElem();
        }
        else
            xmlReader.readNext();
    }
}

void    ONSMessenger::readRecordElem()
{
    xmlReader.readNext();
    while(!xmlReader.atEnd())
    {
        if(xmlReader.isEndElement())
        {
            xmlReader.readNext();
            break;
        }
        if(xmlReader.isStartElement())
        {
            if(xmlReader.name()=="ServiceType")
                readServiceTypeElem();
            else  if(xmlReader.name()=="Status")
                readStatusElem();
            else  if(xmlReader.name()=="Description")
                readDescriptionElem();
            else  if(xmlReader.name()=="URI")
                readURIElem();
            else  if(xmlReader.name()=="ID")
                readIDElem();
            else  if(xmlReader.name()=="GW-IP")
                readGWIPElem();
            else  if(xmlReader.name()=="GW-Port")
                readGWPortElem();
            else  if(xmlReader.name()=="PassWord")
                readPassWordElem();
            else
                skipUnknownElem();
        }
        else
            xmlReader.readNext();
    }
}

//目前看来，不会出现调用该函数的情况
void ONSMessenger::readClientNameElem()
{
    xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void  ONSMessenger::readDescriptionElem()
{
    Description=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

//目前来看，应当不会出现调用该函数的情况
void    ONSMessenger::readGWURIElem()
{
     xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readIDElem()
{
    ID=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readGWIPElem()
{
    GWIP=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readGWPortElem()
{
    GWPort=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readPassWordElem()
{
    PassWord=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readSerialNumberElem()
{
    bool ok;
    SerialNumer=xmlReader.readElementText().toUShort(&ok,10);
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readServiceTypeElem()
{
    ServiceType=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readStatusElem()
{
    Status=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::readURIElem()
{
    URI=xmlReader.readElementText();
    if(xmlReader.isEndElement())
        xmlReader.readNext();
}

void    ONSMessenger::skipUnknownElem()
{
    xmlReader.readNext();
    while(!xmlReader.atEnd())
    {
        if(xmlReader.isEndElement())
        {
            xmlReader.readNext();
            break;
        }
        if(xmlReader.isStartElement())
            skipUnknownElem();
        else
            xmlReader.readNext();
    }
}
