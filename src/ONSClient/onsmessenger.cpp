#include "onsmessenger.h"

ONSMessenger::ONSMessenger(QString iniMyURI, QObject *parent) :
    QObject(parent)
{
    resetDate();
    MyURI=iniMyURI; //初始化自己的URI
    SerialNumer=0;
    XMLBuffer.setData("");
}

/*
  生成用于发送给ONS的消息，把消息存储与xmlBuffer。并且返回这个结果
  */
QByteArray ONSMessenger::genMesg(QString paraURI_ObjName, QString paraURI_ObjType,
    QString paraURI_ObjOwner, QString paraURI_ObjLocation, QString paraServiceType,
                                QString paraID, bool paraIsGW, QString paraPWD)
{
    generateURI(paraURI_ObjName,paraURI_ObjType,paraURI_ObjOwner,
        paraURI_ObjLocation);
    generateXML(paraServiceType,paraID,paraIsGW,paraPWD);
    return XMLBuffer.data();
}

QString  ONSMessenger::analyzeReceivedMsg(QByteArray paraOnsMsg)
{
    XMLBuffer.setData(paraOnsMsg);
    return parseXML();
}

/*
 解析XMLBuffer中的xml文档。
 */
QString    ONSMessenger::parseXML()
{
    resetDate();
    XMLBuffer.open(QIODevice::ReadWrite);

    xmlReader.setDevice(&XMLBuffer);
    xmlReader.readNext();
    while(!xmlReader.atEnd())
    {
        if(xmlReader.isStartElement())
        {
            if(xmlReader.name()=="VideoMonitor")
            readVideoMonitorElem();
            else
            {
                xmlReader.raiseError(QObject::tr("Not VideoMonitor file"));
            }
        }
        else
            xmlReader.readNext();
    }

    XMLBuffer.close();

    if(xmlReader.hasError())
        return xmlReader.errorString();
    else
        return "XML parse Success";
}

/*
  生成一条URI
  */
void    ONSMessenger::generateURI(QString paraURI_ObjName, QString paraURI_ObjType,
                    QString paraURI_ObjOwner, QString paraURI_ObjLocation)
{
    URI="iot://"+paraURI_ObjName+"."+paraURI_ObjType+".WSN.tnsroot.cn/monitor/1/"+
            paraURI_ObjOwner+"/"+paraURI_ObjLocation;
}


/*****
  功能：根据数据域的数据，生成XML文档，结果存储在数据域的XMLBuffer中
  注意：在调用该函数前，确保已经对数据域的数据进行了正确的赋值
  ****/
void    ONSMessenger::generateXML(QString paraServiceType, QString paraID,
                                  bool paraIsGW, QString paraPWD)
{
    XMLBuffer.setData("");
    XMLBuffer.open(QIODevice::ReadWrite);
    xmlWriter.setDevice(&XMLBuffer);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("VideoMonitor"); // ---<VideoMonitor>
    xmlWriter.writeTextElement("SerialNumber",QString::number(SerialNumer));
    if(!paraIsGW)
        xmlWriter.writeTextElement("ClientName",MyURI);
    if(paraIsGW)
        xmlWriter.writeTextElement("GW-URI",MyURI);

    xmlWriter.writeStartElement("Record"); // ---<Record>
    xmlWriter.writeTextElement("ServiceType",paraServiceType);
    if(!URI.isEmpty())
        xmlWriter.writeTextElement("URI",URI);
    if(!paraID.isEmpty())
        xmlWriter.writeTextElement("ID",paraID);
    if(!paraPWD.isEmpty())
        xmlWriter.writeTextElement("PassWord",paraPWD);

    xmlWriter.writeEndElement(); // ---</Record>

    xmlWriter.writeEndElement(); // ---</VideoMonitor>
    xmlWriter.writeEndDocument();

    XMLBuffer.close();
    SerialNumer++;
}

void    ONSMessenger::resetDate()
{
    ApplicationType="videoMonitor";

    ServiceType="Register";
    URI="";
    ID="";
    PassWord="";
    Status="";
    GWIP="";
    GWPort="";
}
