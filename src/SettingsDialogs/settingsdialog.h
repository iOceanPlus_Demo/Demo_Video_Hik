#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
    class SettingsDialog;
}

class MultiCameraSettings;
class QSqlQuery;

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

    Ui::SettingsDialog *ui;
    MultiCameraSettings *cameraSelectDialog;
   // QString tableName; //存储历史检索的视频表格的名称
    //tableNameList存储实时播放的表格的名字。list大小为4，对应播放界面的4个播放窗口。
    QList <QString> tableNameList;
    QSqlQuery   *sqlQueryInSetting;
    void closeEvent(QCloseEvent *);

private:

private slots:

    void on_pushButtonOK_clicked();
    void on_pushButtonConnect_toggled(bool checked);
    void on_pushBtnCameraSeleAdv_clicked();
    void updateTableName(); //更新成员变量“tableName”

    void on_pushButtoGetIPn_clicked();
};

#endif // SETTINGSDIALOG_H
