#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>
#include "settingsdialog.h"
#include "realplay.h"
#include "ui_settingsdialog.h"
#include "db.h"
#include "multicamerasettings.h"
#include "ui_multicamerasettings.h"

#define DBUserName "wsnuser"
#define DBPWD "sensor"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    cameraSelectDialog=new MultiCameraSettings(this);
    sqlQueryInSetting=NULL; //该指针将会在on_pushButtonConnect_toggled()中初始化
    tableNameList.append("");tableNameList.append("");
    tableNameList.append("");tableNameList.append("");
}

void  SettingsDialog::on_pushButtonOK_clicked()
{
    ((RealPlay*)parent())->close=false;
    if(ui->pushButtonConnect->isChecked())
    {
        updateTableName();
        accept();
    }
    else
        QMessageBox::warning(this,tr("提示"),tr("请先单击连接按钮"));
}


void SettingsDialog::updateTableName()
{
    bool ok;
    if(!ui->comboBoxCamera->currentText().isEmpty())
    {
        QString tableNamePrefix=QDate::currentDate().toString("'multi'yyyy_MM_dd_'mac'").toAscii();
        if( ((RealPlay*)parent())->dbo->query->exec("select NodeMacAddress from NodeAddressMap "
              "where NodeName='"+ui->comboBoxCamera->currentText()+"'"))
        {
            if( ((RealPlay*)parent())->dbo->query->next())
            {
                 quint64 macAddr=((RealPlay*)parent())->dbo->query->value(0).toULongLong(&ok);
                tableNameList.replace(0,tableNamePrefix+QString::number(macAddr,10));
                ((RealPlay*)parent())->ui.frame0->macAddr=macAddr;
            }
            else
                qDebug()<<"no NodeMacAddress match nodeName";
        }
        else
            qDebug()<<"select NodeMacAddress fail"<<((RealPlay*)parent())->dbo->query->lastError();
    }
    else
    {
        tableNameList.replace(0,"");
        ((RealPlay*)parent())->ui.frame0->macAddr=0;
    }

}


void SettingsDialog::on_pushButtonConnect_toggled(bool checked)
{
    QRegExp r("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b");
    QString ip=ui->lineEditHost->text();
    if(!ip.contains(r)&&ui->pushButtonConnect->isChecked())
    {
        QMessageBox::warning(this,tr("Ip地址无效"),tr("请重新获得或输入IP"));
        ui->pushButtonConnect->setChecked(false);
        return;
    }
    if(checked)
    {
        if(!((RealPlay*)parent())->dbo->createConnection(ui->lineEditDatabase->text(),ui->lineEditHost->text(),
                                  DBUserName,DBPWD))
        {
            qDebug() << "connection cFromPlayer failed"<<endl;
            return;
        }
           else
        {
             qDebug() << "connection cFromPlayer successful"<<endl;
             ((RealPlay*)parent())->dbo->isConnected=true;
             ui->pushButtonConnect->setText(tr("断开"));
             qDebug()<<"d";
             ((RealPlay*)parent())->ui.pushButton_realplay->setText("RealPlay");
             ((RealPlay*)parent())->ui.pushButton_realplay->setDisabled(false);
             qDebug()<<"dd";
             //连接建立后，初始化sqlQueryInSetting.连接名“cFromPlayer”是写死在createConnection函数中的
             sqlQueryInSetting=new QSqlQuery(QSqlDatabase::database("cFromPlayer"));
         }

           QString today,tomorrow;
           today=QDate::currentDate().toString("yyyyMMdd");
           tomorrow=QDate::currentDate().addDays(1).toString("yyyyMMdd");

           if(!sqlQueryInSetting->exec("select NodeName from nodeaddressmap where NodeOnWork=1 and NodeType='ARM' "))
               qDebug()<<"select camera name fail";
           else
           {
               ui->comboBoxCamera->clear();
               cameraSelectDialog->ui->comboBoxCameraSele0->clear();
               cameraSelectDialog->ui->comboBoxCameraSele1->clear();
               cameraSelectDialog->ui->comboBoxCameraSele2->clear();
               cameraSelectDialog->ui->comboBoxCameraSele3->clear();
               cameraSelectDialog->ui->comboBoxCameraSele0->addItem("");
               cameraSelectDialog->ui->comboBoxCameraSele1->addItem("");
               cameraSelectDialog->ui->comboBoxCameraSele2->addItem("");
               cameraSelectDialog->ui->comboBoxCameraSele3->addItem("");


               while(sqlQueryInSetting->next())
               {
                  ui->comboBoxCamera->addItem(sqlQueryInSetting->value(0).toString());
                  cameraSelectDialog->ui->comboBoxCameraSele0->addItem(sqlQueryInSetting->value(0).toString());
                  cameraSelectDialog->ui->comboBoxCameraSele1->addItem(sqlQueryInSetting->value(0).toString());
                  cameraSelectDialog->ui->comboBoxCameraSele2->addItem(sqlQueryInSetting->value(0).toString());
                  cameraSelectDialog->ui->comboBoxCameraSele3->addItem(sqlQueryInSetting->value(0).toString());
                }
           }
    }
    else
    {

        QSqlDatabase db=QSqlDatabase::database("cFromPlayer");
        db.close();
        ((RealPlay*)parent())->dbo->isConnected=false;
        ui->pushButtonConnect->setText(tr("连接"));
    }
}

void SettingsDialog::on_pushBtnCameraSeleAdv_clicked()
{
    if(!ui->pushButtonConnect->isChecked())
        QMessageBox::warning(this,tr("提示"),tr("请先单击连接按钮"));
    else
    {
        cameraSelectDialog->ui->comboBoxCameraSele0->setCurrentIndex(
                ui->comboBoxCamera->currentIndex()+1);
        cameraSelectDialog->exec();
    }
}

void SettingsDialog::closeEvent(QCloseEvent *)
{
    if(((RealPlay*)parent())->close)
        QTimer::singleShot(0,((RealPlay*)parent()),SLOT(slotTimerQuit()));
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}


void SettingsDialog::on_pushButtoGetIPn_clicked()
{
      ((RealPlay*)parent())->setHostIP();
}
