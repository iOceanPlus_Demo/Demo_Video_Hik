#ifndef SEARCHTIMESET_H
#define SEARCHTIMESET_H

#include <QDialog>

namespace Ui {
    class SearchTimeSet;
}
class QSqlQuery;

class SearchTimeSet : public QDialog
{
    Q_OBJECT

public:
    explicit SearchTimeSet(QWidget *parent = 0);
    ~SearchTimeSet();
    Ui::SearchTimeSet *ui;
    QString tableNameForHistory;
    qreal   rowRate; //视频播放时，每秒播放的行数。由SearchTimeSet对这个值进行更新
    //validBeginVideoID是validBegin时间对于的表格中的ID。根据它和rowRate，可以计算出某个时间的视频的ID
    quint32 validBeginVideoID;
    QList <quint32> validBeginVideoIDList;
private:

    QSqlQuery *queryOfSerch;
    QSqlQuery   *queryOfSerch2;
    QString timeBegin,timeEnd;
    //comboBoxValidBegin和comboBoxValidEnd,有一个改变时，另一个也作相应改变。为了
    //防止出现死循环，用该flag来控制
    bool    flagForComboBoxChange;
    void updateTableName();

private slots:
    void on_buttonBox_accepted();
    void on_timeEditEnd_editingFinished();
    void on_pushButtonRefresh_clicked();
    void on_dateEdit_editingFinished();
    void on_comboBoxValidBegin_currentIndexChanged(int index);
    void on_comboBoxValidEnd_currentIndexChanged(int index);


};

#endif // SEARCHTIMESET_H
