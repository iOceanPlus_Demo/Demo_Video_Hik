#ifndef MULTICAMERASETTINGS_H
#define MULTICAMERASETTINGS_H

#include <QDialog>

namespace Ui {
    class MultiCameraSettings;
}
class QSqlQuery;
class RealPlay;

class MultiCameraSettings : public QDialog
{
    Q_OBJECT

public:
    explicit MultiCameraSettings(QDialog *parent = 0);
    ~MultiCameraSettings();
    Ui::MultiCameraSettings *ui;
    QSqlQuery   *sqlQueryInMultiCameraSet;
    RealPlay    *pointerToMyGrandPaRealPlay;

private:

private slots:
   void on_pushButtonOk_clicked();
   void on_pushButtonCancel_clicked();

};

#endif // MULTICAMERASETTINGS_H
