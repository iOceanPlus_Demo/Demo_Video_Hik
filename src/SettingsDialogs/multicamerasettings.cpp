#include <QSqlQuery>
#include <QSqlError>
#include <QDate>
#include <QDebug>
#include "realplay.h"
#include "ui_realplay.h"
#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "multicamerasettings.h"
#include "ui_multicamerasettings.h"

MultiCameraSettings::MultiCameraSettings(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::MultiCameraSettings)
{
    ui->setupUi(this);
    sqlQueryInMultiCameraSet=NULL;
    pointerToMyGrandPaRealPlay=(RealPlay*)(parent->parent()); //指向主窗口RealPlay的指针
}

/*
更新tableNameList.
对祖父对象realPlay的孙子frame的数据成员macAddr进行更新
*/
void MultiCameraSettings::on_pushButtonOk_clicked()
{
    bool ok;
    sqlQueryInMultiCameraSet=((SettingsDialog*)parent())->sqlQueryInSetting;
    QString tableNamePrefix=QDate::currentDate().toString("'multi'yyyy_MM_dd_'mac'").toAscii();
    qint16  tempIndex=ui->comboBoxCameraSele0->currentIndex()-1;
    if(tempIndex>(((SettingsDialog*)parent())->ui->comboBoxCamera->count()-1))
        tempIndex=((SettingsDialog*)parent())->ui->comboBoxCamera->count()-1;
    else if(tempIndex<-1)
        tempIndex=-1;

    ((SettingsDialog*)parent())->ui->comboBoxCamera->setCurrentIndex(tempIndex);

    //先清空tableNameList
    ((SettingsDialog*)parent())->tableNameList.replace(0,"");
    ((SettingsDialog*)parent())->tableNameList.replace(1,"");
    ((SettingsDialog*)parent())->tableNameList.replace(2,"");
    ((SettingsDialog*)parent())->tableNameList.replace(3,"");
    pointerToMyGrandPaRealPlay->ui.frame0->macAddr=0;
    pointerToMyGrandPaRealPlay->ui.frame_1->macAddr=0;
    pointerToMyGrandPaRealPlay->ui.frame_2->macAddr=0;
    pointerToMyGrandPaRealPlay->ui.frame_3->macAddr=0;

    if(!ui->comboBoxCameraSele0->currentText().isEmpty()&&sqlQueryInMultiCameraSet->exec("select NodeMacAddress from NodeAddressMap "
          "where NodeName='"+ui->comboBoxCameraSele0->currentText()+"'"))
    {
        if( sqlQueryInMultiCameraSet->next())
        {
            quint64 macAddr=sqlQueryInMultiCameraSet->value(0).toULongLong(&ok);
            pointerToMyGrandPaRealPlay->ui.frame0->macAddr=macAddr;
            ((SettingsDialog*)parent())->tableNameList.replace(0,tableNamePrefix+QString::number(macAddr,10));
        }
        else
            qDebug()<<"no NodeMacAddress match nodeName";          
    }
    else
    {
        qDebug()<<"select NodeMacAddress fail or empty name"<<sqlQueryInMultiCameraSet->lastError();
    }
    if(!ui->comboBoxCameraSele1->currentText().isEmpty()&&sqlQueryInMultiCameraSet->exec("select NodeMacAddress from NodeAddressMap "
          "where NodeName='"+ui->comboBoxCameraSele1->currentText()+"'"))
    {
        if( sqlQueryInMultiCameraSet->next())
        {
            quint64 macAddr=sqlQueryInMultiCameraSet->value(0).toULongLong(&ok);
            pointerToMyGrandPaRealPlay->ui.frame_1->macAddr=macAddr;

            ((SettingsDialog*)parent())->tableNameList.replace(1,tableNamePrefix+QString::number(macAddr,10));        }
        else
            qDebug()<<"no NodeMacAddress match nodeName";
    }
    else if(!ui->comboBoxCameraSele1->currentText().isEmpty())
        qDebug()<<"select NodeMacAddress fail or empty name"<<sqlQueryInMultiCameraSet->lastError();


    if(!ui->comboBoxCameraSele2->currentText().isEmpty()&&sqlQueryInMultiCameraSet->exec("select NodeMacAddress from NodeAddressMap "
          "where NodeName='"+ui->comboBoxCameraSele2->currentText()+"'"))
    {
        if( sqlQueryInMultiCameraSet->next())
        {
            quint64 macAddr=sqlQueryInMultiCameraSet->value(0).toULongLong(&ok);
            pointerToMyGrandPaRealPlay->ui.frame_2->macAddr=macAddr;
            ((SettingsDialog*)parent())->tableNameList.replace(2,tableNamePrefix+QString::number(macAddr,10));        }
        else
            qDebug()<<"no NodeMacAddress match nodeName";
    }
    else if(!ui->comboBoxCameraSele2->currentText().isEmpty())
        qDebug()<<"select NodeMacAddress fail or empty name"<<sqlQueryInMultiCameraSet->lastError();

    if(!ui->comboBoxCameraSele3->currentText().isEmpty()&&sqlQueryInMultiCameraSet->exec("select NodeMacAddress from NodeAddressMap "
          "where NodeName='"+ui->comboBoxCameraSele3->currentText()+"'"))
    {
        if( sqlQueryInMultiCameraSet->next())
        {
            quint64 macAddr=sqlQueryInMultiCameraSet->value(0).toULongLong(&ok);
            pointerToMyGrandPaRealPlay->ui.frame_3->macAddr=macAddr;
            ((SettingsDialog*)parent())->tableNameList.replace(3,tableNamePrefix+QString::number(macAddr,10));        }
        else
            qDebug()<<"no NodeMacAddress match nodeName";
    }
    else if(!ui->comboBoxCameraSele3->currentText().isEmpty())
        qDebug()<<"select NodeMacAddress fail or empty name"<<sqlQueryInMultiCameraSet->lastError();

    accept();

}

void MultiCameraSettings::on_pushButtonCancel_clicked()
{
    reject();
}

MultiCameraSettings::~MultiCameraSettings()
{
    delete ui;
}
