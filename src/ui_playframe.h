/********************************************************************************
** Form generated from reading UI file 'playframe.ui'
**
** Created: Wed Oct 31 20:48:34 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYFRAME_H
#define UI_PLAYFRAME_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_PlayFrame
{
public:

    void setupUi(QFrame *PlayFrame)
    {
        if (PlayFrame->objectName().isEmpty())
            PlayFrame->setObjectName(QString::fromUtf8("PlayFrame"));
        PlayFrame->resize(400, 255);
        PlayFrame->setFrameShape(QFrame::StyledPanel);
        PlayFrame->setFrameShadow(QFrame::Raised);

        retranslateUi(PlayFrame);

        QMetaObject::connectSlotsByName(PlayFrame);
    } // setupUi

    void retranslateUi(QFrame *PlayFrame)
    {
        PlayFrame->setWindowTitle(QApplication::translate("PlayFrame", "Frame", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PlayFrame: public Ui_PlayFrame {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYFRAME_H
