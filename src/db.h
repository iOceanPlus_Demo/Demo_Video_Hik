/******************************************
  提供几个数据库操作的函数
  ******************************************/
#ifndef DB_H
#define DB_H
#include <QtCore>
class QSqlQuery;
class databaseOperation : public QObject
{
    Q_OBJECT
public:
    databaseOperation(QObject* parent=0);
    bool createConnection(const QString & dataBase,const QString & host,
        const QString & user, const QString & passWord); //建立一个连接

    QSqlQuery* query;
    bool  isConnected;
public slots:
    void almostQuit(); //和abouttoquit（）相连

private:

};
#endif // DB_H
