#ifndef DEBUGINFO_H
#define DEBUGINFO_H

#include <QDialog>

namespace Ui {
    class DebugInfo;
}

class DebugInfo : public QDialog
{
    Q_OBJECT

public:
    explicit DebugInfo(QDialog *parent = 0);
    ~DebugInfo();
    Ui::DebugInfo *ui;

private:

};

#endif // DEBUGINFO_H
