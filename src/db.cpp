/*******************************************************
    提供几个数据库操作的函数.
    在连接刚刚建立时，清空AliveCmdRec表。
连接断开前，也清空AliveCmdRec表。
  *****************************************************/
#include "db.h"
#include <QMessageBox>
#include <QtSql>
databaseOperation::databaseOperation(QObject* parent)
    :QObject(parent)
{
connect( qApp ,SIGNAL(aboutToQuit()), this, SLOT(almostQuit()) );

isConnected=false;
}

/***************************************************
建立与数据库的连接.连接名为“cFromNetServer".
获得该连接的方法：
QSqlDatabase db=QSqlDatabase::database("cFromNetServer");
相应的，要用下面的语句来定义一个QSqlQuery。
QSqlQuery query(QSqlDatabase::database("cFromNetServer"));
******************************************************/
bool databaseOperation::createConnection(const QString & dataBase,const QString & host,
   const QString & user, const QString & passWord) //建立一个连接
{
     //plugin的搜索路径增加当前目录下的plugins文件夹。该语句主要为了发布之便。
    qApp->addLibraryPath(qApp->applicationDirPath () + "/plugins");
    qApp->addLibraryPath(qApp->applicationDirPath () + "/lib");

    //添加驱动
    QSqlDatabase connectionFromServer=QSqlDatabase::addDatabase("QMYSQL","cFromPlayer");
    connectionFromServer.setHostName( host);//数据库所在主机的IP
    connectionFromServer.setDatabaseName(dataBase);//操作example数据库
    if(!connectionFromServer.open(user,passWord))
    {
        QMessageBox::critical(0,QObject::tr("Database Error"),
                             connectionFromServer.lastError().text());
        return false;
    }
    else
    {
        query=new QSqlQuery(QSqlDatabase::database("cFromPlayer"));
         query->setForwardOnly(true);
         qDebug()<<"Create DB connection success";
    }
    return true;
}


/******程序将要退出前的清理工作，如断开数据库连接*******/
void databaseOperation::almostQuit()
{

   if(isConnected)
    {
        delete query; query=NULL;
        QSqlDatabase db=QSqlDatabase::database("cFromPlayer");
        db.close();
        if(!db.isOpen())
            ;

    #ifdef debu
        qDebug()<<"connection cFromPlayer disconnected";
    #endif
    }
}




