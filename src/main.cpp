
#include <QtGui/QApplication>
#include <QTextCodec>
#include "RealPlay/realplay.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec *textc=QTextCodec::codecForName("utf8");
    QTextCodec::setCodecForTr(QTextCodec::codecForName("system"));
    QTextCodec::setCodecForCStrings(textc);

     QTextCodec::setCodecForLocale(QTextCodec::codecForName("GB2312"));
    RealPlay r;

    return a.exec();
}
