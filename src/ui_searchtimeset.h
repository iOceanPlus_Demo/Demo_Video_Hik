/********************************************************************************
** Form generated from reading UI file 'searchtimeset.ui'
**
** Created: Wed Oct 31 20:48:34 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCHTIMESET_H
#define UI_SEARCHTIMESET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateEdit>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SearchTimeSet
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QDateEdit *dateEdit;
    QLabel *label_3;
    QComboBox *comboBoxNodeName;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButtonRefresh;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_7;
    QComboBox *comboBoxValidBegin;
    QLabel *label;
    QComboBox *comboBoxValidEnd;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label_4;
    QTimeEdit *timeEditBegin;
    QSpacerItem *horizontalSpacer;
    QLabel *label_5;
    QTimeEdit *timeEditEnd;
    QSpacerItem *verticalSpacer_2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SearchTimeSet)
    {
        if (SearchTimeSet->objectName().isEmpty())
            SearchTimeSet->setObjectName(QString::fromUtf8("SearchTimeSet"));
        SearchTimeSet->resize(490, 239);
        QFont font;
        font.setPointSize(11);
        SearchTimeSet->setFont(font);
        verticalLayout = new QVBoxLayout(SearchTimeSet);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_6 = new QLabel(SearchTimeSet);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_5->addWidget(label_6);

        dateEdit = new QDateEdit(SearchTimeSet);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(5);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(dateEdit->sizePolicy().hasHeightForWidth());
        dateEdit->setSizePolicy(sizePolicy);
        dateEdit->setDateTime(QDateTime(QDate(2012, 10, 18), QTime(0, 0, 0)));
        dateEdit->setCalendarPopup(true);

        horizontalLayout_5->addWidget(dateEdit);

        label_3 = new QLabel(SearchTimeSet);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_5->addWidget(label_3);

        comboBoxNodeName = new QComboBox(SearchTimeSet);
        comboBoxNodeName->setObjectName(QString::fromUtf8("comboBoxNodeName"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(5);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboBoxNodeName->sizePolicy().hasHeightForWidth());
        comboBoxNodeName->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(comboBoxNodeName);

        horizontalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(29, 29, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        pushButtonRefresh = new QPushButton(SearchTimeSet);
        pushButtonRefresh->setObjectName(QString::fromUtf8("pushButtonRefresh"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/new/prefix1/icons/interact.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonRefresh->setIcon(icon);
        pushButtonRefresh->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(pushButtonRefresh);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_7 = new QLabel(SearchTimeSet);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_2->addWidget(label_7);

        comboBoxValidBegin = new QComboBox(SearchTimeSet);
        comboBoxValidBegin->setObjectName(QString::fromUtf8("comboBoxValidBegin"));

        horizontalLayout_2->addWidget(comboBoxValidBegin);

        label = new QLabel(SearchTimeSet);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        label->setMinimumSize(QSize(50, 30));
        label->setStyleSheet(QString::fromUtf8("border-image: url(:/new/prefix1/icons/forward .png);"));

        horizontalLayout_2->addWidget(label);

        comboBoxValidEnd = new QComboBox(SearchTimeSet);
        comboBoxValidEnd->setObjectName(QString::fromUtf8("comboBoxValidEnd"));

        horizontalLayout_2->addWidget(comboBoxValidEnd);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalSpacer = new QSpacerItem(20, 18, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_4 = new QLabel(SearchTimeSet);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout->addWidget(label_4);

        timeEditBegin = new QTimeEdit(SearchTimeSet);
        timeEditBegin->setObjectName(QString::fromUtf8("timeEditBegin"));
        sizePolicy.setHeightForWidth(timeEditBegin->sizePolicy().hasHeightForWidth());
        timeEditBegin->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(timeEditBegin);

        horizontalSpacer = new QSpacerItem(18, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        label_5 = new QLabel(SearchTimeSet);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout->addWidget(label_5);

        timeEditEnd = new QTimeEdit(SearchTimeSet);
        timeEditEnd->setObjectName(QString::fromUtf8("timeEditEnd"));
        sizePolicy.setHeightForWidth(timeEditEnd->sizePolicy().hasHeightForWidth());
        timeEditEnd->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(timeEditEnd);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer_2 = new QSpacerItem(20, 18, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        buttonBox = new QDialogButtonBox(SearchTimeSet);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SearchTimeSet);
        QObject::connect(buttonBox, SIGNAL(accepted()), SearchTimeSet, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SearchTimeSet, SLOT(reject()));

        QMetaObject::connectSlotsByName(SearchTimeSet);
    } // setupUi

    void retranslateUi(QDialog *SearchTimeSet)
    {
        SearchTimeSet->setWindowTitle(QApplication::translate("SearchTimeSet", "Dialog", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("SearchTimeSet", "\346\227\245\346\234\237\357\274\232", 0, QApplication::UnicodeUTF8));
        dateEdit->setDisplayFormat(QApplication::translate("SearchTimeSet", "yyyy-MM-dd", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SearchTimeSet", "\350\212\202\347\202\271\345\220\215\357\274\232", 0, QApplication::UnicodeUTF8));
        pushButtonRefresh->setText(QString());
        label_7->setText(QApplication::translate("SearchTimeSet", "\346\234\211\346\225\210\346\227\266\351\227\264\346\256\265\357\274\232", 0, QApplication::UnicodeUTF8));
        label->setText(QString());
        label_4->setText(QApplication::translate("SearchTimeSet", "  \350\265\267\345\247\213\346\227\266\351\227\264\357\274\232", 0, QApplication::UnicodeUTF8));
        timeEditBegin->setDisplayFormat(QApplication::translate("SearchTimeSet", "HH:mm:ss", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SearchTimeSet", "\347\273\223\346\235\237\346\227\266\351\227\264\357\274\232", 0, QApplication::UnicodeUTF8));
        timeEditEnd->setDisplayFormat(QApplication::translate("SearchTimeSet", "HH:mm:ss", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SearchTimeSet: public Ui_SearchTimeSet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCHTIMESET_H
