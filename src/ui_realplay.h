/********************************************************************************
** Form generated from reading UI file 'realplay.ui'
**
** Created: Wed Oct 31 20:50:39 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REALPLAY_H
#define UI_REALPLAY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QTimeEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "playframe.h"

QT_BEGIN_NAMESPACE

class Ui_RealPlayClass
{
public:
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_20;
    QGridLayout *gridLayout;
    PlayFrame *frame0;
    PlayFrame *frame_1;
    PlayFrame *frame_2;
    PlayFrame *frame_3;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *pushButton_realplay;
    QPushButton *pushButton_search;
    QPushButton *pushButton_settings;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButtonShowTime;
    QPushButton *pushButtonFullScreen;
    QGroupBox *groupBoxPlayBack;
    QHBoxLayout *horizontalLayout_19;
    QLabel *labelBeginTime;
    QSlider *horizontalSlider;
    QLabel *labelEndTime;
    QTimeEdit *timeEditCurrent;

    void setupUi(QWidget *RealPlayClass)
    {
        if (RealPlayClass->objectName().isEmpty())
            RealPlayClass->setObjectName(QString::fromUtf8("RealPlayClass"));
        RealPlayClass->resize(350, 352);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(RealPlayClass->sizePolicy().hasHeightForWidth());
        RealPlayClass->setSizePolicy(sizePolicy);
        RealPlayClass->setMinimumSize(QSize(0, 0));
        RealPlayClass->setMaximumSize(QSize(1600, 1200));
        QFont font;
        font.setFamily(QString::fromUtf8("WenQuanYi Zen Hei"));
        RealPlayClass->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/new/prefix1/icons/camera_video.png"), QSize(), QIcon::Normal, QIcon::Off);
        RealPlayClass->setWindowIcon(icon);
        verticalLayout_5 = new QVBoxLayout(RealPlayClass);
        verticalLayout_5->setSpacing(2);
        verticalLayout_5->setContentsMargins(2, 2, 2, 2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(2);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame0 = new PlayFrame(RealPlayClass);
        frame0->setObjectName(QString::fromUtf8("frame0"));
        frame0->setMinimumSize(QSize(20, 28));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
        frame0->setPalette(palette);
        frame0->setCursor(QCursor(Qt::OpenHandCursor));
        frame0->setAutoFillBackground(false);
        frame0->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/icons/vlc_media_player.png);\n"
"background-color: rgb(0, 0, 0);"));
        frame0->setFrameShape(QFrame::Panel);
        frame0->setFrameShadow(QFrame::Raised);
        frame0->setLineWidth(0);

        gridLayout->addWidget(frame0, 0, 0, 1, 1);

        frame_1 = new PlayFrame(RealPlayClass);
        frame_1->setObjectName(QString::fromUtf8("frame_1"));
        frame_1->setMinimumSize(QSize(20, 28));
        frame_1->setCursor(QCursor(Qt::OpenHandCursor));
        frame_1->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/icons/vlc_media_player.png);\n"
"background-color: rgb(0, 0, 0);"));
        frame_1->setFrameShape(QFrame::Panel);
        frame_1->setFrameShadow(QFrame::Raised);
        frame_1->setLineWidth(0);

        gridLayout->addWidget(frame_1, 0, 1, 1, 1);

        frame_2 = new PlayFrame(RealPlayClass);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(20, 28));
        frame_2->setCursor(QCursor(Qt::OpenHandCursor));
        frame_2->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/icons/vlc_media_player.png);\n"
"background-color: rgb(0, 0, 0);"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Raised);
        frame_2->setLineWidth(0);

        gridLayout->addWidget(frame_2, 1, 0, 1, 1);

        frame_3 = new PlayFrame(RealPlayClass);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setMinimumSize(QSize(20, 28));
        frame_3->setCursor(QCursor(Qt::OpenHandCursor));
        frame_3->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/icons/vlc_media_player.png);\n"
"background-color: rgb(0, 0, 0);"));
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Raised);
        frame_3->setLineWidth(0);

        gridLayout->addWidget(frame_3, 1, 1, 1, 1);


        horizontalLayout_20->addLayout(gridLayout);

        horizontalLayout_20->setStretch(0, 10);

        verticalLayout_5->addLayout(horizontalLayout_20);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(2);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        pushButton_realplay = new QPushButton(RealPlayClass);
        pushButton_realplay->setObjectName(QString::fromUtf8("pushButton_realplay"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButton_realplay->sizePolicy().hasHeightForWidth());
        pushButton_realplay->setSizePolicy(sizePolicy1);
        pushButton_realplay->setMaximumSize(QSize(70, 22));
        pushButton_realplay->setCheckable(true);

        horizontalLayout_13->addWidget(pushButton_realplay);

        pushButton_search = new QPushButton(RealPlayClass);
        pushButton_search->setObjectName(QString::fromUtf8("pushButton_search"));
        pushButton_search->setMaximumSize(QSize(80, 22));
        pushButton_search->setCheckable(true);

        horizontalLayout_13->addWidget(pushButton_search);

        pushButton_settings = new QPushButton(RealPlayClass);
        pushButton_settings->setObjectName(QString::fromUtf8("pushButton_settings"));
        pushButton_settings->setMaximumSize(QSize(40, 22));

        horizontalLayout_13->addWidget(pushButton_settings);

        horizontalSpacer_7 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_7);

        pushButtonShowTime = new QPushButton(RealPlayClass);
        pushButtonShowTime->setObjectName(QString::fromUtf8("pushButtonShowTime"));
        pushButtonShowTime->setMaximumSize(QSize(80, 22));
        pushButtonShowTime->setCheckable(true);

        horizontalLayout_13->addWidget(pushButtonShowTime);

        pushButtonFullScreen = new QPushButton(RealPlayClass);
        pushButtonFullScreen->setObjectName(QString::fromUtf8("pushButtonFullScreen"));
        pushButtonFullScreen->setMaximumSize(QSize(40, 22));
        pushButtonFullScreen->setCheckable(true);

        horizontalLayout_13->addWidget(pushButtonFullScreen);


        verticalLayout_5->addLayout(horizontalLayout_13);

        groupBoxPlayBack = new QGroupBox(RealPlayClass);
        groupBoxPlayBack->setObjectName(QString::fromUtf8("groupBoxPlayBack"));
        groupBoxPlayBack->setMinimumSize(QSize(0, 30));
        groupBoxPlayBack->setMaximumSize(QSize(16777215, 20));
        horizontalLayout_19 = new QHBoxLayout(groupBoxPlayBack);
        horizontalLayout_19->setSpacing(2);
        horizontalLayout_19->setContentsMargins(2, 2, 2, 2);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        labelBeginTime = new QLabel(groupBoxPlayBack);
        labelBeginTime->setObjectName(QString::fromUtf8("labelBeginTime"));

        horizontalLayout_19->addWidget(labelBeginTime);

        horizontalSlider = new QSlider(groupBoxPlayBack);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setMinimumSize(QSize(10, 0));
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setTickPosition(QSlider::NoTicks);

        horizontalLayout_19->addWidget(horizontalSlider);

        labelEndTime = new QLabel(groupBoxPlayBack);
        labelEndTime->setObjectName(QString::fromUtf8("labelEndTime"));

        horizontalLayout_19->addWidget(labelEndTime);

        timeEditCurrent = new QTimeEdit(groupBoxPlayBack);
        timeEditCurrent->setObjectName(QString::fromUtf8("timeEditCurrent"));
        timeEditCurrent->setMinimumSize(QSize(0, 15));

        horizontalLayout_19->addWidget(timeEditCurrent);


        verticalLayout_5->addWidget(groupBoxPlayBack);

        verticalLayout_5->setStretch(0, 100);
        verticalLayout_5->setStretch(1, 1);
        verticalLayout_5->setStretch(2, 1);

        retranslateUi(RealPlayClass);

        QMetaObject::connectSlotsByName(RealPlayClass);
    } // setupUi

    void retranslateUi(QWidget *RealPlayClass)
    {
        RealPlayClass->setWindowTitle(QApplication::translate("RealPlayClass", "\350\247\206\351\242\221\347\233\221\346\216\247", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        RealPlayClass->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        pushButton_realplay->setText(QApplication::translate("RealPlayClass", "Reaplay", 0, QApplication::UnicodeUTF8));
        pushButton_search->setText(QApplication::translate("RealPlayClass", "\345\233\236\346\224\276", 0, QApplication::UnicodeUTF8));
        pushButton_settings->setText(QApplication::translate("RealPlayClass", "\350\256\276\347\275\256", 0, QApplication::UnicodeUTF8));
        pushButtonShowTime->setText(QApplication::translate("RealPlayClass", "\346\230\276\347\244\272\350\277\233\345\272\246", 0, QApplication::UnicodeUTF8));
        pushButtonFullScreen->setText(QApplication::translate("RealPlayClass", "\345\205\250\345\261\217", 0, QApplication::UnicodeUTF8));
        groupBoxPlayBack->setTitle(QString());
        labelBeginTime->setText(QApplication::translate("RealPlayClass", "TextLabel", 0, QApplication::UnicodeUTF8));
        labelEndTime->setText(QApplication::translate("RealPlayClass", "TextLabel", 0, QApplication::UnicodeUTF8));
        timeEditCurrent->setDisplayFormat(QApplication::translate("RealPlayClass", "HH:mm:ss", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RealPlayClass: public Ui_RealPlayClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REALPLAY_H
