/********************************************************************************
** Form generated from reading UI file 'multicamerasettings.ui'
**
** Created: Wed Oct 31 20:48:34 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MULTICAMERASETTINGS_H
#define UI_MULTICAMERASETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MultiCameraSettings
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QFrame *frame0;
    QComboBox *comboBoxCameraSele0;
    QFrame *frame_1;
    QComboBox *comboBoxCameraSele1;
    QFrame *frame_2;
    QComboBox *comboBoxCameraSele2;
    QFrame *frame_3;
    QComboBox *comboBoxCameraSele3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonOk;
    QPushButton *pushButtonCancel;

    void setupUi(QWidget *MultiCameraSettings)
    {
        if (MultiCameraSettings->objectName().isEmpty())
            MultiCameraSettings->setObjectName(QString::fromUtf8("MultiCameraSettings"));
        MultiCameraSettings->resize(480, 480);
        MultiCameraSettings->setMinimumSize(QSize(480, 480));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        QBrush brush1(QColor(113, 111, 100, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Active, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        QBrush brush2(QColor(118, 116, 108, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        MultiCameraSettings->setPalette(palette);
        QFont font;
        font.setPointSize(16);
        MultiCameraSettings->setFont(font);
        verticalLayout = new QVBoxLayout(MultiCameraSettings);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame0 = new QFrame(MultiCameraSettings);
        frame0->setObjectName(QString::fromUtf8("frame0"));
        QFont font1;
        font1.setPointSize(9);
        frame0->setFont(font1);
        frame0->setAutoFillBackground(false);
        frame0->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"image: url(:/new/prefix1/icons/vlc_media_player.png);"));
        frame0->setFrameShape(QFrame::Panel);
        frame0->setFrameShadow(QFrame::Raised);
        frame0->setLineWidth(1);
        frame0->setMidLineWidth(2);
        comboBoxCameraSele0 = new QComboBox(frame0);
        comboBoxCameraSele0->setObjectName(QString::fromUtf8("comboBoxCameraSele0"));
        comboBoxCameraSele0->setGeometry(QRect(30, 80, 171, 31));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush3(QColor(0, 0, 0, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        comboBoxCameraSele0->setPalette(palette1);
        QFont font2;
        font2.setFamily(QString::fromUtf8("\351\273\221\344\275\223"));
        font2.setPointSize(18);
        comboBoxCameraSele0->setFont(font2);
        comboBoxCameraSele0->setIconSize(QSize(0, 0));
        comboBoxCameraSele0->setFrame(true);

        gridLayout->addWidget(frame0, 0, 0, 1, 1);

        frame_1 = new QFrame(MultiCameraSettings);
        frame_1->setObjectName(QString::fromUtf8("frame_1"));
        frame_1->setFont(font1);
        frame_1->setAutoFillBackground(false);
        frame_1->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"image: url(:/new/prefix1/icons/vlc_media_player.png);"));
        frame_1->setFrameShape(QFrame::Panel);
        frame_1->setFrameShadow(QFrame::Raised);
        frame_1->setLineWidth(1);
        frame_1->setMidLineWidth(2);
        comboBoxCameraSele1 = new QComboBox(frame_1);
        comboBoxCameraSele1->setObjectName(QString::fromUtf8("comboBoxCameraSele1"));
        comboBoxCameraSele1->setGeometry(QRect(30, 80, 171, 31));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Button, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        comboBoxCameraSele1->setPalette(palette2);
        comboBoxCameraSele1->setFont(font2);
        comboBoxCameraSele1->setIconSize(QSize(0, 0));
        comboBoxCameraSele1->setFrame(true);

        gridLayout->addWidget(frame_1, 0, 1, 1, 1);

        frame_2 = new QFrame(MultiCameraSettings);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setFont(font1);
        frame_2->setAutoFillBackground(false);
        frame_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"image: url(:/new/prefix1/icons/vlc_media_player.png);"));
        frame_2->setFrameShape(QFrame::Panel);
        frame_2->setFrameShadow(QFrame::Raised);
        frame_2->setLineWidth(1);
        frame_2->setMidLineWidth(2);
        comboBoxCameraSele2 = new QComboBox(frame_2);
        comboBoxCameraSele2->setObjectName(QString::fromUtf8("comboBoxCameraSele2"));
        comboBoxCameraSele2->setGeometry(QRect(30, 80, 171, 31));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush);
        palette3.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        comboBoxCameraSele2->setPalette(palette3);
        comboBoxCameraSele2->setFont(font2);
        comboBoxCameraSele2->setIconSize(QSize(0, 0));
        comboBoxCameraSele2->setFrame(true);

        gridLayout->addWidget(frame_2, 1, 0, 1, 1);

        frame_3 = new QFrame(MultiCameraSettings);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFont(font1);
        frame_3->setAutoFillBackground(false);
        frame_3->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"image: url(:/new/prefix1/icons/vlc_media_player.png);"));
        frame_3->setFrameShape(QFrame::Panel);
        frame_3->setFrameShadow(QFrame::Raised);
        frame_3->setLineWidth(1);
        frame_3->setMidLineWidth(2);
        comboBoxCameraSele3 = new QComboBox(frame_3);
        comboBoxCameraSele3->setObjectName(QString::fromUtf8("comboBoxCameraSele3"));
        comboBoxCameraSele3->setGeometry(QRect(30, 80, 171, 31));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush);
        palette4.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::ButtonText, brush);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        comboBoxCameraSele3->setPalette(palette4);
        comboBoxCameraSele3->setFont(font2);
        comboBoxCameraSele3->setIconSize(QSize(0, 0));
        comboBoxCameraSele3->setFrame(true);

        gridLayout->addWidget(frame_3, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButtonOk = new QPushButton(MultiCameraSettings);
        pushButtonOk->setObjectName(QString::fromUtf8("pushButtonOk"));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette5.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        pushButtonOk->setPalette(palette5);
        pushButtonOk->setDefault(true);
        pushButtonOk->setFlat(false);

        horizontalLayout->addWidget(pushButtonOk);

        pushButtonCancel = new QPushButton(MultiCameraSettings);
        pushButtonCancel->setObjectName(QString::fromUtf8("pushButtonCancel"));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Text, brush3);
        palette6.setBrush(QPalette::Active, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Active, QPalette::ButtonText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Text, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::ButtonText, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette6.setBrush(QPalette::Disabled, QPalette::BrightText, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        pushButtonCancel->setPalette(palette6);

        horizontalLayout->addWidget(pushButtonCancel);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout->setStretch(0, 60);

        retranslateUi(MultiCameraSettings);

        QMetaObject::connectSlotsByName(MultiCameraSettings);
    } // setupUi

    void retranslateUi(QWidget *MultiCameraSettings)
    {
        MultiCameraSettings->setWindowTitle(QApplication::translate("MultiCameraSettings", "\346\221\204\345\203\217\345\244\264\351\200\211\346\213\251", 0, QApplication::UnicodeUTF8));
        pushButtonOk->setText(QApplication::translate("MultiCameraSettings", "\347\241\256\345\256\232", 0, QApplication::UnicodeUTF8));
        pushButtonCancel->setText(QApplication::translate("MultiCameraSettings", "\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MultiCameraSettings: public Ui_MultiCameraSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MULTICAMERASETTINGS_H
