/********************************************************************************
** Form generated from reading UI file 'debuginfo.ui'
**
** Created: Wed Oct 31 20:48:35 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGINFO_H
#define UI_DEBUGINFO_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugInfo
{
public:
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *plainTextEdit;

    void setupUi(QWidget *DebugInfo)
    {
        if (DebugInfo->objectName().isEmpty())
            DebugInfo->setObjectName(QString::fromUtf8("DebugInfo"));
        DebugInfo->resize(274, 210);
        verticalLayout = new QVBoxLayout(DebugInfo);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        plainTextEdit = new QPlainTextEdit(DebugInfo);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        verticalLayout->addWidget(plainTextEdit);


        retranslateUi(DebugInfo);

        QMetaObject::connectSlotsByName(DebugInfo);
    } // setupUi

    void retranslateUi(QWidget *DebugInfo)
    {
        DebugInfo->setWindowTitle(QApplication::translate("DebugInfo", "Form", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DebugInfo: public Ui_DebugInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGINFO_H
