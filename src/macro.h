#ifndef MACRO_H
#define MACRO_H

#define insideHostIP "10.21.0.250" //服务器内部IP
#define publicHostIP "159.226.41.232" //服务器外部IP

#define maxPercantage 0.9
#define minPercantage 0.4
#define ONS_CLIENT_NAME "VideoMonitor"

#define sensorIDHik 11
#define sensorIDMJPEG 12


#define ONS_IP "218.241.108.59"
#define ONS_PORT 4446

#define VIDEO_MAX_SENSORID 24
#define VIDEO_MIN_SENSORID 9

#define ServerPortVideo 6788 //服务器的6788端口是发送视频数据的

#define maxWaitTime 5000

#define MAC_BYTES 8
#define SN_BYTES 1
#define SENSORID_BYTES 1

#endif // MACRO_H
