#-------------------------------------------------
#
# Project created by QtCreator 2011-02-21T19:19:48
#
#-------------------------------------------------

QT       += core gui  sql network

TARGET = VideoMoni1031NoTitle
TEMPLATE = app


SOURCES += main.cpp\
    db.cpp \
    RealPlay/realplay.cpp \
    RealPlay/realplayUi.cpp \
    RealPlay/playctrl.cpp \
    RealPlay/resolveDll.cpp \
    SettingsDialogs/settingsdialog.cpp \
    SettingsDialogs/searchtimeset.cpp \
    SettingsDialogs/multicamerasettings.cpp \
    RealPlay/playframe.cpp \
    debuginfo.cpp \
    ONSClient/onsmessenger.cpp \
    ONSClient/readXMLElem.cpp

HEADERS  +=  db.h \
    RealPlay/realplay.h \
    RealPlay/playctrl.h \
    PlayCtrl6.2.1.1/PlayM4.h \
    RealPlay/resolveDll.h \
    SettingsDialogs/settingsdialog.h \
    SettingsDialogs/searchtimeset.h \
    SettingsDialogs/multicamerasettings.h \
    RealPlay/playframe.h \
    macro.h \
    debuginfo.h \
    ONSClient/onsmessenger.h \
    VideoHik.h


FORMS    +=  \
    RealPlay/realplay.ui \
    SettingsDialogs/searchtimeset.ui \
    SettingsDialogs/settingsdialog.ui \
    SettingsDialogs/multicamerasettings.ui \
    RealPlay/playframe.ui \
    debuginfo.ui

OTHER_FILES += \
    icon.rc

INCLUDEPATH += PlayCtrl6.2.1.1 \
                RealPlay\
                SettingsDialogs

#CONFIG    += staticlib \
 #       dll

RESOURCES += \
    icons.qrc

 RC_FILE = icon.rc





