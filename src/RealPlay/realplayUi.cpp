#include <QDebug>
#include <QMessageBox>
//#include "playsdkpu.h"
#include "PlayM4.h"
#include "realplay.h"
#include "playframe.h"
#include <QMessageBox>
#include <QTimer>
#include <QSqlQuery>
#include <QUdpSocket>
#include <QMouseEvent>
#include "searchtimeset.h"
#include "ui_searchtimeset.h"
#include "db.h"
#include "playctrl.h"
#include "settingsdialog.h"
#include "playframe.h"
#include "debuginfo.h"
#include "macro.h"
#define serverPort 6789  //udp端口

void RealPlay::initFrameIndex()
{
    ui.frame0->frameIndex=0;ui.frame_1->frameIndex=1;
    ui.frame_2->frameIndex=2;ui.frame_3->frameIndex=3;
}

void RealPlay::slotShowMax(int index)
{
    QList <PlayFrame*> listOfFrame;
    listOfFrame.append(ui.frame0);
    listOfFrame.append(ui.frame_1);
    listOfFrame.append(ui.frame_2);
    listOfFrame.append(ui.frame_3);

    bool portUsed[5]={port0Used,port1Used,port2Used,port3Used,port10Used};

    if( listOfFrame.at(0)->isHidden()||listOfFrame.at(1)->isHidden() ) //有个播放窗口最大化的情况
    {
        for(int n=0;n<=3;n++)
        {
            listOfFrame.at(n)->setPixmap(QPixmap());
        }
        for(int n=0;n<=3;n++)
        {
           listOfFrame.at(n)->show();
        }
        for(int n=0;n<=3;n++)
        {
            if(portUsed[n]&&sensorIDList.at(n)==11)
                funSetPicQuality(n,true);
        }
        if(port10Used&&sensorIDList.at(0)==11)
            funSetPicQuality(10,true);

    }
    else
    {
        for(int n=0;n<=3;n++)
        {
           if(n!=index)
               listOfFrame.at(n)->hide();
           else
               listOfFrame.at(n)->show();
        }

        for(int n=0;n<=3;n++)
        {
            if(portUsed[n]&&n!=index&&sensorIDList.at(n)==11)
                funSetPicQuality(n,true);
            else if(n==index&&playCtrl->onRealPlay&&sensorIDList.at(n)==11)
                funSetPicQuality(n,true);
            else if(index==0&&playCtrl->onSearch&&port10Used&&sensorIDList.at(0)==11)
                funSetPicQuality(10,true);
        }

        if(sensorIDList.at(0)==11&&port10Used&&(index!=0||(index==0&&!playCtrl->onSearch)))
            funSetPicQuality(10,true);

    }
}

void RealPlay::on_pushButton_realplay_toggled(bool checked)
{
    if(checked) //启动实时视频
    {
         playCtrl->onSearch=false;
         if(playCtrl->hasBegun&&sensorIDList.at(0)==11) //如果有端口已经打开，则先停止，释放播放端口等资源
         {
             funStop(10);
             funCloseStream(10);
             port10Used=false;
         }
          playCtrl->hasBegun=false;
          playCtrl->onRealPlay=true;

          ui.pushButton_search->setChecked(false);
          openPort();
          playCtrl->hasBegun=true; //palyer has been ready for play


           udpSocket->writeDatagram("INFO:Punch hole",QHostAddress(serverIP),6788);  //hole punching

          if(ui.frame0->macAddr>0&&ui.frame0->isVisible())
          {
              if(sensorIDList.at(0)==sensorIDHik)
		{		
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame0->macAddr,10)+
                  " RealPlayBegin",QHostAddress(serverIP),serverPort);
		}

              else if(sensorIDList.at(0)==sensorIDMJPEG)
		{
                  udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame0->macAddr,10)+
                    " RealPlayBegin",QHostAddress(serverIP),serverPort);
		}
          }
          if(ui.frame_1->macAddr>0&&ui.frame_1->isVisible())
          {
              if(sensorIDList.at(1)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_1->macAddr,10)+
                  " RealPlayBegin",QHostAddress(serverIP),serverPort);
              else if(sensorIDList.at(1)==sensorIDMJPEG)
                  udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_1->macAddr,10)+
                    " RealPlayBegin",QHostAddress(serverIP),serverPort);
          }
          if(ui.frame_2->macAddr>0&&ui.frame_2->isVisible())
          {
              if(sensorIDList.at(2)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_2->macAddr,10)+
                  " RealPlayBegin",QHostAddress(serverIP),serverPort);
              else if(sensorIDList.at(2)==sensorIDMJPEG)
                  udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_2->macAddr,10)+
                    " RealPlayBegin",QHostAddress(serverIP),serverPort);
          }
          if(ui.frame_3->macAddr>0&&ui.frame_3->isVisible())
          {
              if(sensorIDList.at(3)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_3->macAddr,10)+
                  " RealPlayBegin",QHostAddress(serverIP),serverPort);
              else if(sensorIDList.at(3)==sensorIDMJPEG)
                  udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_3->macAddr,10)+
                    " RealPlayBegin",QHostAddress(serverIP),serverPort);
          }

         ui.pushButton_realplay->setText("StopReal");
    }
    else  //停止实时视频
    {
        ui.frame0->setPixmap(QPixmap());
        ui.frame_1->setPixmap(QPixmap());
        ui.frame_2->setPixmap(QPixmap());
        ui.frame_3->setPixmap(QPixmap());
        if(port0Used&&sensorIDList.at(0)==11)
        {
            funStop(0);
            //funCloseStream(0);
            port0Used=false;
        }

        if(port1Used&&sensorIDList.at(1)==11)
        {
            funStop(1);
            //funCloseStream(1);
            port1Used=false;
        }
        if(port2Used&&sensorIDList.at(2)==11)
        {
            funStop(2);
            //funCloseStream(2);
            port2Used=false;
        }
        if(port3Used&&sensorIDList.at(3)==11)
        {
            funStop(3);
            //funCloseStream(3);
            port3Used=false;
        }
        playCtrl->onRealPlay=false;
        ui.pushButton_realplay->setText("RealPlay");
        if(ui.frame0->macAddr>0&&ui.frame0->isVisible())
        {
            if(sensorIDList.at(0)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame0->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
            else if(sensorIDList.at(0)==sensorIDMJPEG)
                udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame0->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
        }
        if(ui.frame_1->macAddr>0&&ui.frame_1->isVisible())
        {
            if(sensorIDList.at(1)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_1->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
            else if(sensorIDList.at(1)==sensorIDMJPEG)
                udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_1->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
        }
        if(ui.frame_2->macAddr>0&&ui.frame_2->isVisible())
        {
            if(sensorIDList.at(2)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_2->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
            else if(sensorIDList.at(2)==sensorIDMJPEG)
                udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_2->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
        }
        if(ui.frame_3->macAddr>0&&ui.frame_3->isVisible())
        {
            if(sensorIDList.at(3)==sensorIDHik)
                udpSocket->writeDatagram("VideoHik  "+QByteArray::number(ui.frame_3->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
            else if(sensorIDList.at(3)==sensorIDMJPEG)
                udpSocket->writeDatagram("VideoMJPEG  "+QByteArray::number(ui.frame_3->macAddr,10)+
                " RealPlayStop",QHostAddress(serverIP),serverPort);
        }

        update();
    }

}

void RealPlay::on_pushButton_search_toggled(bool checked)
{
    //bool ok;
    if(checked)
    {
         playCtrl->onRealPlay=false;
         ui.horizontalSlider->setValue(0);
         if(playCtrl->hasBegun) //如果有端口已经打开，则先停止，释放播放端口等资源
         {
             dataList0.clear();

             if(port0Used&&sensorIDList.at(0)==11)
             {
                 funStop(0);
                 funCloseStream(0);
                 port0Used=false;
             }
             if(port1Used&&sensorIDList.at(1)==11)
             {
                 funStop(1);
                 funCloseStream(1);
                 port1Used=false;
             }
             if(port2Used&&sensorIDList.at(2)==11)
             {
                 funStop(2);
                 funCloseStream(2);
                 port2Used=false;
             }
             if(port3Used&&sensorIDList.at(3)==11)
             {
                 funStop(3);
                 funCloseStream(3);
                 port3Used=false;
             }
         }
         playCtrl->hasBegun=false;

         ui.pushButton_realplay->setChecked(false);
         ui.pushButton_search->setText(tr("停止回放"));
         ui.groupBoxPlayBack->show();

         timeEdit->exec();  //edit time

         secondsOfPlayback=playB->secsTo(*playE);
         quint32 secondsOfGap=QTime::fromString(timeEdit->ui->comboBoxValidBegin->currentText()).secsTo(
                 *playB);
         videoID=timeEdit->validBeginVideoID+timeEdit->rowRate*secondsOfGap;

         if(videoID<timeEdit->validBeginVideoID)
             videoID=timeEdit->validBeginVideoID;

         qDebug()<<"videoID is"<<videoID;
         qDebug()<<"newPlayBack?"<<newPlayBack;

         if(newPlayBack)
         {
             qDebug()<<"sensorIDList.at(0)"<<sensorIDList.at(0);
              openPort();

              newPlayBack=false;
              ui.labelBeginTime->setText( playB->toString("hh:mm:ss"));
              ui.labelEndTime->setText( playE->toString("hh:mm:ss"));
              ui.timeEditCurrent->setTime(*playB);
              playCtrl->onSearch=true;
              playCtrl->hasBegun=true; //palyer has been ready for play

              timerForInputData.start(1000/(timeEdit->rowRate+1));
          }
    }
    else //结束回放
    {
        ui.pushButton_search->setText(tr("回放"));
        ui.groupBoxPlayBack->hide();
         playCtrl->onSearch=false;
         dataList0.clear();
         if(playCtrl->hasBegun&&sensorIDList.at(0)==11) //如果有端口已经打开，则先停止，释放播放端口等资源
         {
             funStop(10);
             funCloseStream(10);
             port10Used=false;
         }

          update();
    }
}

/*
void RealPlay::on_pushButtonStartCam_clicked()
{

    udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_RealPlayStart",QHostAddress(serverIP),serverPort);

}


void RealPlay::on_pushButtonStopCam_clicked()
{
    udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_RealPlayStop",QHostAddress(serverIP),serverPort);
     //udpSocket->waitForReadyRead(1000); //wait for ok
}
*/
void RealPlay::on_horizontalSlider_sliderPressed()
{
    //playCtrl->onSearch=false;
}

void RealPlay::on_horizontalSlider_sliderReleased()
{
    if(playCtrl->onSearch==true)
    {
    horizonValueApplied=false;


    quint64 sec=ui.horizontalSlider->value()/100.0*secondsOfPlayback;

    quint64 secondsOfGap=QTime::fromString(timeEdit->ui->comboBoxValidBegin->currentText()).secsTo(
            *playB);
    videoID=timeEdit->validBeginVideoID+timeEdit->rowRate*(secondsOfGap+sec);
    }
}


void RealPlay::on_pushButton_settings_clicked()
{
    settingsDialog->show();
}


void RealPlay::on_pushButtonFullScreen_toggled(bool checked)
{
    if(checked)
    {
        showFullScreen();
        ui.pushButtonFullScreen->setText(tr("退出全屏"));
    }
    else
    {
        showNormal();
        ui.pushButtonFullScreen->setText(tr("全屏"));
    }
}

/*
void RealPlay::on_pushButtonDebug_clicked()
{
    myDebugInfo->show();
}
*/
