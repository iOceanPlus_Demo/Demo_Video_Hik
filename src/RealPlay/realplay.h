/*
 * Copyright(C) 2010,Custom Co., Ltd
 *    FileName: realplay.h
 * Description: ???
 *     Version: 1.0
 *      Author: wanggongpu
 * Create Date: 2009-11,12
 * Modification History
 *    2010-6-25 panyadong ????
 */
#ifndef REALPLAY_H
#define REALPLAY_H

#include <QtGui/QWidget>
#include <QDialog>
#include <QLibrary>
#include "ui_realplay.h"
#include <QFrame>
#include <QTimer>
#include "resolveDll.h" //---wulin
//??????洢·???

//Max num of windows used to preview.
#define REALPLAY_MAX_NUM 36

class databaseOperation;

class SearchTimeSet;
class PlayCtrl;
class   QUdpSocket;
class SettingsDialog;
class   DebugInfo; //显示debug信息的窗口
class ONSMessenger;

//class RealPlay : public QMainWindow
class RealPlay :public QDialog
{
    Q_OBJECT

public:
    //friend class QtClientDemo;

    RealPlay(QWidget* parent=0);
   // RealPlay(TreeModel**  model, const QModelIndex * index, QList<DeviceData>* tree,
        //	int* userid, NET_DVR_DEVICEINFO_V30* devinfo, int* channelnum,
        //	int* channelmode, LogTable * table, QWidget *parent = 0);
    ~RealPlay();

    void setHostIP();
    void stopRealPlay();
    void initFrameIndex();//初始化4个播放窗口的成员变量Frameindex

    void myRealDataCallBack( QByteArray &videoData,quint8 playPort);

    void showMJPEGVideo(QByteArray &videoData,quint8 windowIndex);
   // void restartTimerForRead(quint16 miliSecs); //根据网络速度作出播放速度调整

    databaseOperation *dbo;
    SearchTimeSet* timeEdit;
    QString searchTimeBegin,searchTimeEnd,searchCurrentTime;

    quint64 videoIDB,videoIDE; //Begin and End of playback
    quint64  secondsOfPlayback;
    QTime *playB,*playE;
    bool newPlayBack;
    QUdpSocket *udpSocket;
    QUdpSocket *blockUdpSocket;
    QString serverIP;

    bool port0Used,port1Used,port2Used,port3Used,port10Used;
    QList <quint8>  sensorIDList; //记录4个窗口分别播放的是哪种类型的视频

    Ui::RealPlayClass ui;
    SettingsDialog *settingsDialog;
    bool  close; //当用户直接从设置对话框退出时，用于关闭程序


protected:

public slots:
    void slotShowMax(int index);

private slots:
    void slotTimerQuit();
    //void on_pushButtonDebug_clicked();
    void on_pushButtonFullScreen_toggled(bool checked);

    void on_pushButton_settings_clicked();

    //void on_pushButtonStopCam_clicked();
    //void on_pushButtonStartCam_clicked();
    void on_horizontalSlider_sliderPressed();
    void on_horizontalSlider_sliderReleased();

    void on_pushButton_search_toggled(bool checked);

    void on_pushButton_realplay_toggled(bool checked);

    void readDatabase(); //read video
    void openPort();  //open port of player, read first two packets

    void processPendingDatagrams();  //read udp packet
    void almostQuit();

    void    timerFunctionForInputData();

    void on_pushButtonShowTime_toggled(bool checked);

private:
    ONSMessenger*  onsMsger;
    QTime timeElapsedForInputData; //用于控制帧率的时钟。

    //用于控制帧率的时钟。一个时钟控制一个窗口的播放
    QList  <QTime*> timeElapForInpt;
    QList  <QPixmap*>  pixmapList;//存储4个窗口需要显示的图片

    void toConnectFrameCtrl(); // 把frame的信号和playCtrl的函数连接
    QTimer *timerForRead;

    // 通过timeForNetWorkMeasure.elapsed()来测试网速，作出播放速度调整
    QTime   timeForNetWorkMeasure;

    quint16 readDataBaseGap; //读数据库的间隔，以ms为单位

    quint64 videoID; //用于历史检索

    QList <QByteArray>  dataList0; //存储4个窗口需要播出的数据
    QTimer timerForInputData; //用于解码的定时器

    bool horizonValueApplied;
    QByteArray *pBuffer;
    //long m_iPort;  //????port
    //QTimer* timerForCtrl;
    PlayCtrl *playCtrl;

    myStop funStop;
    mySetStreamOpenMode funSetStreamOpenMode;
    myInputData funInputData;
    myCloseStream funCloseStream;
    myFreePort  funFreePort;
    myGetPort   funGetPort;
    myOpenStream    funOpenStream;
    myPlay  funPlay;
    mySetColor  funSetColor;
    myGetLastError  funGetLastError;
    mySetPicQuality funSetPicQuality;
    QLibrary lib;

    DebugInfo   *myDebugInfo;

};
#endif // REALPLAY_H




