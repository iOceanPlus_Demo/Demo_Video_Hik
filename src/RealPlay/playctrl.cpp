#include "playctrl.h"
#include <QMessageBox>
#include <QUdpSocket>
#include <QTimer>
#include "realplay.h"
#include "settingsdialog.h"
#define serverPort 6789  //服务器UDP端口

PlayCtrl::PlayCtrl(QObject *parent) :
    QObject(parent)
{
    left=false;right=false;up=false;down=false;
     hasBegun=false;
     onNarrowFocus=false; onExpandFocus=false;
     onFarFocus=false;onNearFocus=false;
     onLargeAperture=false;onSmallAperture=false;
     onRealPlay=false;onSearch=false;
     serverIP=((RealPlay*)parent)->serverIP;
}



bool PlayCtrl::turnUp()
{

    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        up=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart TILT_UP",QHostAddress(serverIP),serverPort);
        return true;
    }
}

void PlayCtrl::slotTurnUp(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        up=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStart TILT_UP",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));
}

bool PlayCtrl::turnDown()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        down=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart TILT_DOWN",QHostAddress(serverIP),serverPort);

         return true;
    }
}

void PlayCtrl::slotTurnDown(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        down=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStart TILT_DOWN",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));
}

bool PlayCtrl::turnLeft()
{

    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();

        return false;
    }

    else
    {
        left=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart PAN_LEFT",QHostAddress(serverIP),serverPort);

         return true;
    }
}

void PlayCtrl::slotTurnLeft(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        left=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStart PAN_LEFT",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));
}

bool PlayCtrl::turnRight()
{

    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        right=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart PAN_RIGHT",QHostAddress(serverIP),serverPort);

         return true;
    }
}

void PlayCtrl::slotTurnRight(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        right=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStart PAN_RIGHT",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));

}

bool PlayCtrl::narrowFocus()
{
   if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onNarrowFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart ZOOM_OUT",QHostAddress(serverIP),serverPort);
        return true;
    }

}

void PlayCtrl::slotNarrowFocus(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        onNarrowFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStart ZOOM_OUT",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));
}

bool PlayCtrl::expandFocus()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onExpandFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart ZOOM_IN",QHostAddress(serverIP),serverPort);
        return true;
    }
}

void PlayCtrl::slotExpandFocus(quint64 macAddr, quint16 msecs)
{
    macAddrForCtrl=macAddr;
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
    }

    else
    {
        onExpandFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+QByteArray::number(macAddr,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStart ZOOM_IN",QHostAddress(serverIP),serverPort);
    }
    QTimer::singleShot(msecs,this,SLOT(slotStopCtrl()));
}

bool    PlayCtrl::nearFocus()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onNearFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart FOCUS_NEAR",QHostAddress(serverIP),serverPort);
        return true;
    }
}

bool    PlayCtrl::farFocus()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onFarFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart FOCUS_FAR",QHostAddress(serverIP),serverPort);
        return true;
    }
}

bool    PlayCtrl::smallAperture()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onFarFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart IRIS_CLOSE",QHostAddress(serverIP),serverPort);
        return true;
    }
}

bool    PlayCtrl::largeAperture()
{
    if(commandOnExcute())  //if some command is in process
    {
        QMessageBox a(QMessageBox::Information,"Notice","Please wait a second",QMessageBox::Ok,(RealPlay*)parent());
        a.exec();
        return false;
    }

    else
    {
        onFarFocus=true;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStart IRIS_OPEN",QHostAddress(serverIP),serverPort);
        return true;
    }
}

bool PlayCtrl::stopCtrl()
{
        if(left)
        {
            left=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop PAN_LEFT",QHostAddress(serverIP),serverPort);
        }
        else if(right)
        {
            right=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop PAN_RIGHT",QHostAddress(serverIP),serverPort);

        }
        else if(up)
        {
            up=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop TILT_UP",QHostAddress(serverIP),serverPort);
        }
        else if(down)
        {
            down=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop TILT_DOWN",QHostAddress(serverIP),serverPort);

        }

        else if(onNarrowFocus)
        {
            onNarrowFocus=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop ZOOM_OUT",QHostAddress(serverIP),serverPort);
        }
        else if(onExpandFocus)
        {
            onExpandFocus=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop ZOOM_IN",QHostAddress(serverIP),serverPort);
        }

        else if(onNearFocus)
        {
            onNearFocus=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop FOCUS_NEAR",QHostAddress(serverIP),serverPort);
        }
        else if(onFarFocus)
        {
            onFarFocus=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop FOCUS_FAR",QHostAddress(serverIP),serverPort);
        }
        else if(onSmallAperture)
        {
            onSmallAperture=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop IRIS_CLOSE",QHostAddress(serverIP),serverPort);
        }
        else if(onLargeAperture)
        {
            onLargeAperture=false;
            ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  15  1  CmdNrm  VideoHik_PTZControlExclusiveStop IRIS_OPEN",QHostAddress(serverIP),serverPort);
        }

   return true;
}

void PlayCtrl::slotStopCtrl()
{
    if(left)
    {
        left=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac "+
              QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop PAN_LEFT",QHostAddress(serverIP),serverPort);
    }
    else if(right)
    {
        right=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
             QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop PAN_RIGHT",QHostAddress(serverIP),serverPort);

    }
    else if(up)
    {
        up=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac "+
                  QByteArray::number(macAddrForCtrl,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStop TILT_UP ",QHostAddress(serverIP),serverPort);
    }
    else if(down)
    {
        down=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
             QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop TILT_DOWN",QHostAddress(serverIP),serverPort);

    }

    else if(onNarrowFocus)
    {
        onNarrowFocus=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
            QByteArray::number(macAddrForCtrl,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStop ZOOM_OUT",QHostAddress(serverIP),serverPort);
    }
    else if(onExpandFocus)
    {
        onExpandFocus=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
               QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop ZOOM_IN",QHostAddress(serverIP),serverPort);
    }

    else if(onNearFocus)
    {
        onNearFocus=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
             QByteArray::number(macAddrForCtrl,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStop FOCUS_NEAR",QHostAddress(serverIP),serverPort);
    }
    else if(onFarFocus)
    {
        onFarFocus=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
             QByteArray::number(macAddrForCtrl,10)+"  1  CmdNrm  VideoHik_PTZControlExclusiveStop FOCUS_FAR",QHostAddress(serverIP),serverPort);
    }
    else if(onSmallAperture)
    {
        onSmallAperture=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac  "+
             QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop IRIS_CLOSE",QHostAddress(serverIP),serverPort);
    }
    else if(onLargeAperture)
    {
        onLargeAperture=false;
        ((RealPlay*)parent())->udpSocket->writeDatagram("Mac "+
               QByteArray::number(macAddrForCtrl,10)+" 1  CmdNrm  VideoHik_PTZControlExclusiveStop IRIS_OPEN",QHostAddress(serverIP),serverPort);
    }
}

bool    PlayCtrl::commandOnExcute()
{
    return left||right||up||down||onExpandFocus||onFarFocus||onLargeAperture||
            onNarrowFocus||onNearFocus||onSmallAperture;
}
