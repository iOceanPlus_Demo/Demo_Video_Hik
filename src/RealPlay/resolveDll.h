#ifndef RESOLVEDLL_H
#define RESOLVEDLL_H

typedef     unsigned int    DWORD;
typedef     unsigned short  WORD;
typedef     unsigned short  USHORT;
typedef     int            LONG;
typedef  	unsigned char	BYTE ;
typedef     int BOOL;
typedef     unsigned int   	UINT;
typedef 	void* 			LPVOID;
typedef 	void* 			HANDLE;
typedef     unsigned int*  LPDWORD;
typedef  unsigned long long UINT64;

typedef char*  PBYTE;
typedef bool (*myStop)(LONG);
typedef bool (*mySetStreamOpenMode)(LONG,DWORD);
typedef bool (*myInputData) (LONG,PBYTE,DWORD);
typedef bool (*myOpenStream) (LONG ,PBYTE ,DWORD ,DWORD);
typedef void (*myPlay) (LONG, HWND);
typedef bool (*myCloseStream) (LONG);
typedef bool (*myFreePort) (LONG);
typedef bool (*myGetPort) (LONG* );
typedef bool (*mySetColor) (LONG nPort, DWORD nRegionNum, int nBrightness, int nContrast, int nSaturation, int nHue );
typedef bool (*myGetLastError) (LONG );

typedef bool (*mySetPicQuality) (LONG nPort,BOOL bHighQuality);

#endif // RESOLVEDLL_H
