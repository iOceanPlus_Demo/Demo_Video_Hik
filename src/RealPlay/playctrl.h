#ifndef PLAYCTRL_H
#define PLAYCTRL_H

#include <QObject>

class PlayCtrl : public QObject
{
    Q_OBJECT
public:
    explicit PlayCtrl(QObject *parent = 0);
    quint64 macAddrForCtrl; //记载当前在控制的摄像头的mac地址.
    bool    left,right,up,down;
    bool    onNarrowFocus,onExpandFocus,onNearFocus,onFarFocus,
    onSmallAperture,onLargeAperture,
    onRealPlay,onSearch,hasBegun;  //record comand status
//hasBegun record whether port has been open and ready for play
    //onRealPlay record whether the state is realplay or history mode.

    //programmer will ensure that only if the command are allowed by
    //current user that those buttons are not disabled. And only one
    //user will have the rights to send command to one certain camera at
    //the same time .

    bool    turnLeft(); bool    turnRight();
    bool    turnUp();   bool    turnDown();
    bool    narrowFocus(); bool    expandFocus();
    bool    nearFocus();    bool    farFocus();
    bool    smallAperture();  bool    largeAperture();

    bool    commandOnExcute(); //return true if any command is on excute

signals:

public slots:
    void slotTurnLeft(quint64 macAddr,quint16 msecs); //调试完成后，完全替代上面的turnLeft()函数。下同
    void slotTurnRight(quint64 macAddr,quint16 msecs);
    void slotTurnUp(quint64 macAddr,quint16 msecs);
    void slotTurnDown(quint64 macAddr,quint16 msecs);
    void slotExpandFocus(quint64 macAddr,quint16 msecs);
    void slotNarrowFocus(quint64 macAddr,quint16 msecs);

    void slotStopCtrl();

    bool    stopCtrl();  //such as stop turn left

private:
    QString serverIP;

};

#endif // PLAYCTRL_H
