#include <qmath.h>
#include <QMouseEvent>
#include <QDebug>
#include <QTimer>
#include <QToolTip>
#include "playframe.h"
#include "realplay.h"
#include "settingsdialog.h"
#include "ui_playframe.h"

PlayFrame::PlayFrame(QWidget *parent) :
    QLabel(parent),
    ui(new Ui::PlayFrame)
{
    ui->setupUi(this);
    macAddr=0;  //测试用，以后修改为正确的mac地址
    //记录鼠标滑轮一共滑过的角度的8倍.普通精度的鼠标，滑动一小格滚轮，该值变化120
    totalDaltaForWheel=0;
    setMouseTracking(true);

    QPalette palette;
    QBrush brush1(QColor(255, 255, 255, 255));
    brush1.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
    palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
    palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
    QToolTip::setPalette(palette);
 }

void PlayFrame::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    emit signalShowMaxFrame(frameIndex);
}

void PlayFrame::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::RightButton&&!(((RealPlay*)parent())->
       settingsDialog->tableNameList.at(frameIndex).isEmpty()))
    mouseStartPosX=event->x();mouseStartPosY=event->y();
}

void PlayFrame::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button()==Qt::RightButton&&!(((RealPlay*)parent())->
           settingsDialog->tableNameList.at(frameIndex).isEmpty()))
    {
        mouseStopPosX=event->x();   mouseStopPosY=event->y();
        if(mouseStopPosX>width()) mouseStopPosX=width();
        if(mouseStopPosY>height()) mouseStopPosY=height();
        qreal xChangeRate=(mouseStopPosX-mouseStartPosX)/(width()+0.0);
        qreal yChangeRate=(mouseStopPosY-mouseStartPosY)/(height()+0.0);
        if(qAbs(xChangeRate)>=qAbs(yChangeRate))  //鼠标主要横向移动
        {
            if(qAbs(xChangeRate)>=0.04)//如果鼠标移动很小，则不作处理
            {
                if(xChangeRate>0)  //鼠标右移
                  emit signalTurnRight(macAddr,100*pow(1.5,10*xChangeRate));
                else
                  emit signalTurnLeft(macAddr,100*pow(1.5,-10*xChangeRate));
            }
        }
        else if(qAbs(xChangeRate)<qAbs(yChangeRate))  //鼠标主要纵向移动
        {
            if(qAbs(yChangeRate)>=0.04)//如果鼠标移动很小，则不作处理
            {
                if(yChangeRate>0)  //鼠标下移。鼠标上下移动的幅度一般较小
                  emit signalTurnDown(macAddr,50*pow(1.5,10*yChangeRate));
                else  //
                  emit signalTurnUp(macAddr,50*pow(1.5,-10*yChangeRate));
            }
        }
    }
}

void PlayFrame::wheelEvent(QWheelEvent *event)
{    
    if(totalDaltaForWheel==0)
    QTimer::singleShot(500,this,SLOT(timerEventForFrame()));
    totalDaltaForWheel+=event->delta();
}

void PlayFrame::mouseMoveEvent(QMouseEvent *event)
{
    if(!QToolTip::isVisible())
    QToolTip::showText(event->globalPos(),tr("转向：按住鼠标右键，并滑动鼠标。\n"
        "调焦：滚动鼠标滚轮。\n说明：以上操作只在实时监控状态下有效。"),
        this,QRect());

}

//调用该槽函数时，意意味着鼠标滑轮的转动暂时停止了
void PlayFrame::timerEventForFrame()
{
    if(totalDaltaForWheel>0&&!(((RealPlay*)parent())->
        settingsDialog->tableNameList.at(frameIndex).isEmpty()))
        emit signalExpandFocus(macAddr,totalDaltaForWheel);
    else if(totalDaltaForWheel<0&&!(((RealPlay*)parent())->
              settingsDialog->tableNameList.at(frameIndex).isEmpty()))
        emit signalNarrowFocus(macAddr,-totalDaltaForWheel);

    //qDebug()<<"totalDataForWheel is"<<totalDaltaForWheel;
      totalDaltaForWheel=0;
}

PlayFrame::~PlayFrame()
{
    delete ui;
}
