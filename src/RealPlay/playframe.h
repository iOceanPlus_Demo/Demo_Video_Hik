#ifndef PLAYFRAME_H
#define PLAYFRAME_H

#include <QLabel>

namespace Ui {
    class PlayFrame;
}

class PlayFrame : public QLabel
{
    Q_OBJECT

public:
    explicit PlayFrame(QWidget *parent = 0);
    ~PlayFrame();
    quint8 frameIndex; //范围是0-3，对应于4个播放窗口。在RealPlay的构造函数中，对该值初始化
    quint64 macAddr;
    qint16 totalDaltaForWheel; //鼠标滚轮滚过的总度数的8倍
private:
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    Ui::PlayFrame *ui;
    quint16 mouseStartPosX,mouseStartPosY,mouseStopPosX,mouseStopPosY;

signals:
    //下面的这些信号将和PlayCtrl类的对应槽函数相连接
    void signalShowMaxFrame(int index);
    void signalTurnRight(quint64 nodeMacAddr,quint16 mSecs);
    void signalTurnLeft(quint64 nodeMacAddr,quint16 mSecs);
    void signalTurnUp(quint64 nodeMacAddr,quint16 mSecs);
    void signalTurnDown(quint64 nodeMacAddr,quint16 mSecs);
    void signalNarrowFocus(quint64 nodeMacAddr,quint16 mSecs);
    void signalExpandFocus(quint64 nodeMacAddr,quint16 mSecs);

private slots:
    void timerEventForFrame();


};

#endif // PLAYFRAME_H
