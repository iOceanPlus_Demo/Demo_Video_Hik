/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created: Wed Oct 31 20:48:34 2012
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_8;
    QLineEdit *lineEditURI;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLineEdit *lineEditHost;
    QPushButton *pushButtoGetIPn;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_7;
    QLineEdit *lineEditDatabase;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QComboBox *comboBoxDelay;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButtonConnect;
    QLabel *label_2;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QComboBox *comboBoxCamera;
    QPushButton *pushBtnCameraSeleAdv;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButtonOK;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
        SettingsDialog->resize(607, 270);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        SettingsDialog->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/new/prefix1/icons/camera_video.png"), QSize(), QIcon::Normal, QIcon::Off);
        SettingsDialog->setWindowIcon(icon);
        verticalLayout = new QVBoxLayout(SettingsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_8 = new QLabel(SettingsDialog);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_5->addWidget(label_8);

        lineEditURI = new QLineEdit(SettingsDialog);
        lineEditURI->setObjectName(QString::fromUtf8("lineEditURI"));
        lineEditURI->setMinimumSize(QSize(300, 0));

        horizontalLayout_5->addWidget(lineEditURI);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(SettingsDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);
        label_3->setMaximumSize(QSize(150, 16777215));
        label_3->setLayoutDirection(Qt::LeftToRight);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label_3);

        lineEditHost = new QLineEdit(SettingsDialog);
        lineEditHost->setObjectName(QString::fromUtf8("lineEditHost"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lineEditHost->sizePolicy().hasHeightForWidth());
        lineEditHost->setSizePolicy(sizePolicy1);
        lineEditHost->setMaximumSize(QSize(500, 16777215));

        horizontalLayout->addWidget(lineEditHost);

        pushButtoGetIPn = new QPushButton(SettingsDialog);
        pushButtoGetIPn->setObjectName(QString::fromUtf8("pushButtoGetIPn"));
        pushButtoGetIPn->setEnabled(false);

        horizontalLayout->addWidget(pushButtoGetIPn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        label_7 = new QLabel(SettingsDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        sizePolicy.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy);
        label_7->setMaximumSize(QSize(0, 0));

        horizontalLayout->addWidget(label_7);

        lineEditDatabase = new QLineEdit(SettingsDialog);
        lineEditDatabase->setObjectName(QString::fromUtf8("lineEditDatabase"));
        sizePolicy1.setHeightForWidth(lineEditDatabase->sizePolicy().hasHeightForWidth());
        lineEditDatabase->setSizePolicy(sizePolicy1);
        lineEditDatabase->setMaximumSize(QSize(0, 16777215));

        horizontalLayout->addWidget(lineEditDatabase);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(SettingsDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(102, 0));
        label->setMaximumSize(QSize(102, 16777215));
        label->setBaseSize(QSize(0, 0));
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(label);

        comboBoxDelay = new QComboBox(SettingsDialog);
        comboBoxDelay->setObjectName(QString::fromUtf8("comboBoxDelay"));
        comboBoxDelay->setMinimumSize(QSize(120, 0));
        comboBoxDelay->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_3->addWidget(comboBoxDelay);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        pushButtonConnect = new QPushButton(SettingsDialog);
        pushButtonConnect->setObjectName(QString::fromUtf8("pushButtonConnect"));
        pushButtonConnect->setCheckable(true);

        horizontalLayout_3->addWidget(pushButtonConnect);


        verticalLayout->addLayout(horizontalLayout_3);

        label_2 = new QLabel(SettingsDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font1.setPointSize(10);
        font1.setBold(false);
        font1.setWeight(50);
        label_2->setFont(font1);

        verticalLayout->addWidget(label_2);

        verticalSpacer_3 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(SettingsDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(102, 0));
        label_4->setMaximumSize(QSize(102, 16777215));
        label_4->setBaseSize(QSize(0, 0));
        label_4->setLayoutDirection(Qt::LeftToRight);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(label_4);

        comboBoxCamera = new QComboBox(SettingsDialog);
        comboBoxCamera->setObjectName(QString::fromUtf8("comboBoxCamera"));
        comboBoxCamera->setMinimumSize(QSize(120, 0));
        comboBoxCamera->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_4->addWidget(comboBoxCamera);

        pushBtnCameraSeleAdv = new QPushButton(SettingsDialog);
        pushBtnCameraSeleAdv->setObjectName(QString::fromUtf8("pushBtnCameraSeleAdv"));

        horizontalLayout_4->addWidget(pushBtnCameraSeleAdv);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);

        pushButtonOK = new QPushButton(SettingsDialog);
        pushButtonOK->setObjectName(QString::fromUtf8("pushButtonOK"));

        horizontalLayout_4->addWidget(pushButtonOK);


        verticalLayout->addLayout(horizontalLayout_4);

        verticalSpacer_4 = new QSpacerItem(20, 13, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_4);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_8);


        retranslateUi(SettingsDialog);

        comboBoxDelay->setCurrentIndex(2);
        comboBoxCamera->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "\350\256\276\347\275\256", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("SettingsDialog", "\347\275\221\345\205\263URI\357\274\232", 0, QApplication::UnicodeUTF8));
        lineEditURI->setText(QApplication::translate("SettingsDialog", "iot://gwict.gw.wsn.tnsroot.cn/monitor/1/\347\275\221\347\273\234\346\212\200\346\234\257\347\240\224\347\251\266\344\270\255\345\277\203/\350\256\241\347\256\227\346\211\200", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SettingsDialog", "\347\275\221\345\205\263IP \357\274\232", 0, QApplication::UnicodeUTF8));
        lineEditHost->setText(QApplication::translate("SettingsDialog", "159.226.41.232", 0, QApplication::UnicodeUTF8));
        pushButtoGetIPn->setText(QApplication::translate("SettingsDialog", "\350\216\267\345\217\226\345\205\254\347\275\221IP", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("SettingsDialog", "\346\225\260\346\215\256\345\272\223\345\220\215\357\274\232", 0, QApplication::UnicodeUTF8));
        lineEditDatabase->setText(QApplication::translate("SettingsDialog", "example", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SettingsDialog", "\345\273\266\350\277\237\357\274\232       ", 0, QApplication::UnicodeUTF8));
        comboBoxDelay->clear();
        comboBoxDelay->insertItems(0, QStringList()
         << QApplication::translate("SettingsDialog", "\346\234\200\345\260\217\345\273\266\350\277\237", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialog", "\350\276\203\345\260\217\345\273\266\350\277\237", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialog", "\346\231\256\351\200\232", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialog", "\350\276\203\345\244\247\345\273\266\350\277\237", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("SettingsDialog", "\351\253\230\345\273\266\350\277\237", 0, QApplication::UnicodeUTF8)
        );
        pushButtonConnect->setText(QApplication::translate("SettingsDialog", "\350\277\236\346\216\245", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SettingsDialog", "\346\263\250\357\274\232\345\273\266\350\277\237\350\266\212\345\244\247\357\274\214\346\212\226\345\212\250\350\266\212\345\260\217\343\200\202\350\246\201\344\275\277\345\273\266\350\277\237\347\232\204\350\256\276\347\275\256\347\224\237\346\225\210\357\274\214\345\205\210\345\215\225\345\207\273stopRealplay\357\274\214\n"
"\345\206\215\345\215\225\345\207\273startRealplay\343\200\202", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SettingsDialog", "\346\221\204\345\203\217\345\244\264\351\200\211\346\213\251\357\274\232  ", 0, QApplication::UnicodeUTF8));
        pushBtnCameraSeleAdv->setText(QApplication::translate("SettingsDialog", "\351\253\230\347\272\247", 0, QApplication::UnicodeUTF8));
        pushButtonOK->setText(QApplication::translate("SettingsDialog", "OK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
